package cv;
import com.opencsv.CSVReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CV {

  public static void main(String[] args) {
    try {
      String strFile = "C:\\Users\\USER\\Downloads\\cvdata1.csv";
      CSVReader reader = new CSVReader(new FileReader(strFile));
      String [] nextLine;
      List<String> refs = new ArrayList<String>();
      
      while ((nextLine = reader.readNext()) != null) {
        String email = nextLine[4];
        String referees = nextLine[5];
        refs.add(referees);
      
        System.out.println("{");
        System.out.println("Candidate Email: "+email);
        System.out.println("Referee name: ");
        String s = referees;
        //Etracting referee names
        String lineCsv1 = referees.split(";")[0];
        System.out.println(lineCsv1);     
        Matcher g = Pattern.compile("\\&(.*?)\\;").matcher(s);        
        while (g.find()){
            String names = g.group().replaceAll("&&","");
            System.out.println(names);
        }
        //Etracting referee emails
        Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(s);
        System.out.println("Referee email: ");
        while (m.find()){
            System.out.println(m.group());
        }
        //Extracting phone numbers
        Matcher p = Pattern.compile("([\\.\\-\\s+\\/()]*[0-9][\\.\\-\\s+\\/()]*){10,15}").matcher(s);
        System.out.println("Referee phone: ");
        while (p.find()){
            System.out.println(p.group());
        }
        System.out.println("}");
      }
    }
    catch(IOException e)
    {
     e.printStackTrace();
    }
    }
}
